package GUIMenu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MenuRGB extends JFrame {
	private JFrame frame;
	private JMenuBar menubar;
	private JMenu menu;
	private JPanel r1,b1,g1;
	private JPanel main,cardPanel;
	private CardLayout layoutmain;
	private JMenuItem r,g,b;
	
	public MenuRGB() {
		createframe();
	}
	
	public void createframe() {
		frame = new JFrame();
		frame.setSize(1000, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		menubar = new JMenuBar();
		menu = new JMenu("ChangeRGB");
		menubar.add(menu);
		r = new JMenuItem("RED");
		g = new JMenuItem("GREEN");
		b = new JMenuItem("BLUE");
		menu.add(r);
		menu.add(g);
		menu.add(b);
		
		
		main = new JPanel();
		main.setLayout(new BorderLayout());
		frame.setJMenuBar(menubar);
		
		
		layoutmain = new CardLayout();
		cardPanel = new JPanel();
		cardPanel.setLayout(layoutmain);
		
		r1 = new JPanel();
		r1.setBackground(new Color(255, 0, 0));
		g1 = new JPanel();
		g1.setBackground(new Color(0, 255, 0));
		b1 = new JPanel();
		b1.setBackground(new Color(0, 0, 255));
		
		cardPanel.add(r1,"R");
		cardPanel.add(g1,"G");
		cardPanel.add(b1,"B");
		
		main.add(cardPanel,BorderLayout.CENTER);
		frame.getContentPane().add(main);
		
		
	}
	
	public void setListener(ActionListener addlistlistener) {
		r.addActionListener(addlistlistener);
		g.addActionListener(addlistlistener);
		b.addActionListener(addlistlistener);
	}
	
	public CardLayout getLayout() {
		return layoutmain;
	}
	public JPanel getCardpanel() {
		return cardPanel;
	}
	public JMenu getMenu() {
		return menu;
	}

}
