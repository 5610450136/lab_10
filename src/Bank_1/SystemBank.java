package Bank_1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SystemBank {
	BankUI frame;
	BankAccount b1;
	
	SystemBank() {
		frame = new BankUI();
		frame.setlistener(new addlistListeners());
		testcase();
	}
	
	public void testcase() {
		b1 = new BankAccount("John Wick");
	}
	
	class addlistListeners implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			switch(e.getActionCommand()) {
			case "DEPOSITE" :
				frame.getLayout().show(frame.getCardpanel(), "Deposite");
				break;
			case "WITHDRAW" :
				frame.getLayout().show(frame.getCardpanel(), "Withdraw");
				break;
			case "CHECKBALANCE" :
				setBalance("Balance : " + Double.toString(b1.getBalance()));
				break;
			case "SUBMIT : DEPOSITE" :
				b1.deposite(frame.getDepositeAMount());
				frame.clearTextAmount();
				break;
			case "SUBMIT : WITHDRAW" :
				b1.withdraw(frame.getWithdrawAMount());
				frame.clearTextAmount();
				break;
			}
			
				
		}
		
	}
	public void setBalance(String s) {
		frame.setBalanceResult(s);
	}
}
